import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../pokemon';
import { PokeShareInfoService } from '../poke-share-info.service';
import { PokeApiServicesService } from '../poke-api-services.service';

@Component({
  selector: 'app-pokemon-display',
  templateUrl: './pokemon-display.component.html',
  styleUrls: ['./pokemon-display.component.css'],
  providers: []
})
export class PokemonDisplayComponent {
  @Input('displayedPokemon')
  displayedPokemon!: Pokemon;
  pokemonToDisplay!: Pokemon;
  data: any;
  options: any;
  dataRadar : any;
  shiny : boolean = false;
  shinyUrl: string="";

  

  constructor(private pokeShareInfoService : PokeShareInfoService, private RestService : PokeApiServicesService){
    this.pokeShareInfoService.getVObservableValue().subscribe(value => {
      this.displayedPokemon=value;
      this.updateRadarStats();
      this.updateShiny();
    });

  }
  ngOnInit(): void{
    this.updateRadarStats();
  }

  updateRadarStats():void{
    this.data = {
      labels: ['HP', 'Attaque', 'Defence', 'Attaque spé', 'Defence spé', 'Vitesse'],
      datasets: [
          {
              label: 'Les stats de :' + this.displayedPokemon.name,
              data: [this.displayedPokemon.stats.HP, this.displayedPokemon.stats.attack, this.displayedPokemon.stats.defense, this.displayedPokemon.stats.special_attack, this.displayedPokemon.stats.special_defense, this.displayedPokemon.stats.speed],
              backgroundColor: 'blue',
              borderColor: 'lightgreen',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'lightgreen',
          }
      ]
  };
    this.options = {
      scale: {
          ticks: {
            beginAtZero: true,
            max: 150,
            min: 0,
            stepSize: 15
          }
      }
    };
  }
  
  getNextPokemon():void{
    var idToGet : number = this.displayedPokemon.id+1;
    if(this.displayedPokemon.id<898){
      this.RestService.getPokemonById(idToGet).subscribe((data)=>{
        this.pokemonToDisplay= new Pokemon(data['id'], data['name'], data['apiGeneration'], data['pokedexId'], data['image'], data['sprite'], data['slug'], data['stats'], data['apiTypes'], data['apiResistance'], data['resistanceModifyingAbilitiesForApi'], data['apiEvolutions'], data['apiPreEvolution'], data['apiResistancesWithAbilities']);
        this.pokeShareInfoService.setValue(this.pokemonToDisplay);
        this.updateShiny();
      })
    }
  }
  getPreviousPokemon():void{
    var idToGet : number = this.displayedPokemon.id-1;
    if(this.displayedPokemon.id>1){
      this.RestService.getPokemonById(idToGet).subscribe((data)=>{
        this.pokemonToDisplay= new Pokemon(data['id'], data['name'], data['apiGeneration'], data['pokedexId'], data['image'], data['sprite'], data['slug'], data['stats'], data['apiTypes'], data['apiResistance'], data['resistanceModifyingAbilitiesForApi'], data['apiEvolutions'], data['apiPreEvolution'], data['apiResistancesWithAbilities']);
        this.pokeShareInfoService.setValue(this.pokemonToDisplay);
        this.updateShiny();
      })
    }
  }
  togleShiny():void{
    this.shiny = !this.shiny;
    this.updateShiny();
  }
  updateShiny():void{
    this.shinyUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/"+ this.displayedPokemon.id + ".png";
    console.log("the new shiny url is "+ this.shinyUrl);
  }

}
 