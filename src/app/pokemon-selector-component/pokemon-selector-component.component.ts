import { Component } from '@angular/core';
import { Pokemon } from '../pokemon';
import { PokeApiServicesService } from '../poke-api-services.service';
import { HttpClient } from '@angular/common/http';
import {PokeShareInfoService} from '../poke-share-info.service'

@Component({
  selector: 'app-pokemon-selector-component',
  templateUrl: './pokemon-selector-component.component.html',
  styleUrls: ['./pokemon-selector-component.component.css'],
  providers: []
  
})
export class PokemonSelectorComponentComponent {
  pokeliste : Pokemon[] = [];
  SelectedPokeId: string='';
  SelectedPokeName: string='';
  selectedPokemonToDisplay: Pokemon | undefined;

 
  constructor(private RestService : PokeApiServicesService, private pokeShareInfoService : PokeShareInfoService){
  }

  ngOnInit(): void {
      this.RestService.getPokemons().subscribe((data)=>{
        data['forEach']((e:any) => {
          this.pokeliste.push(new Pokemon(e.id, e.name, e.apiGeneration, e.pokedexId, e.image, e.sprite, e.slug, e.stats, e.apiTypes, e.apiResistances, e.resistanceModifyingAbilitiesForApi, e.apiEvolutions, e.apiPreEvolution, e.apiResistancesWithAbilities));
        });
      });
      //this.random();
  }
  go(){
    this.RestService.getSpecificPokemon(this.SelectedPokeName).subscribe((data)=>{
      this.selectedPokemonToDisplay=new Pokemon(data['id'], data['name'], data['apiGeneration'], data['pokedexId'], data['image'], data['sprite'], data['slug'], data['stats'], data['apiTypes'], data['apiResistance'], data['resistanceModifyingAbilitiesForApi'], data['apiEvolutions'], data['apiPreEvolution'], data['apiResistancesWithAbilities']);
      this.pokeShareInfoService.setValue(this.selectedPokemonToDisplay);
      this.clearInput();
    })
  }
  random(){
    var pokemonId:number = Math.floor(Math.random() * (898 - 1 + 1) + 1);
    this.RestService.getPokemonById(pokemonId).subscribe((data)=>{
      this.selectedPokemonToDisplay=new Pokemon(data['id'], data['name'], data['apiGeneration'], data['pokedexId'], data['image'], data['sprite'], data['slug'], data['stats'], data['apiTypes'], data['apiResistance'], data['resistanceModifyingAbilitiesForApi'], data['apiEvolutions'], data['apiPreEvolution'], data['apiResistancesWithAbilities']);
      this.pokeShareInfoService.setValue(this.selectedPokemonToDisplay);
      this.clearInput();
    })
  }
  clear(){
    this.selectedPokemonToDisplay = undefined;
  }
  clearInput(){
    this.SelectedPokeName="";
  }
}
