import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonSelectorComponentComponent } from './pokemon-selector-component.component';

describe('PokemonSelectorComponentComponent', () => {
  let component: PokemonSelectorComponentComponent;
  let fixture: ComponentFixture<PokemonSelectorComponentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PokemonSelectorComponentComponent]
    });
    fixture = TestBed.createComponent(PokemonSelectorComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
