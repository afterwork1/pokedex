import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Pokemon } from './pokemon';

@Injectable({
  providedIn: 'root' // à ajouter pour que ça soit un singleton 
})
export class PokeShareInfoService {

  private value = new Subject<Pokemon>();

  // Creer un observer du sujet Value
  getVObservableValue():Subject<Pokemon>{
    return this.value;
  }

  // Ici on met à jour la valeur en lui disant que la nouvelle valeur devient celle fournit
  setValue(val: Pokemon){
    this.value.next(val);
  }
  constructor() { }
}
