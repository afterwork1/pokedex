import { TestBed } from '@angular/core/testing';

import { PokeApiServicesService } from './poke-api-services.service';

describe('PokeApiServicesService', () => {
  let service: PokeApiServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokeApiServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
