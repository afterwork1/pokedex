import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const baseUrl:string = "https://pokebuildapi.fr/api/v1/pokemon/"
@Injectable({
  providedIn: 'root'
})

export class PokeApiServicesService {
  [x: string]: any;


  constructor(private http: HttpClient) { 
  }


  getPokemons(): Observable<PokeApiServicesService>{
    return this.http.get<PokeApiServicesService>(baseUrl);
    return this.http.get<PokeApiServicesService>(baseUrl+"limit/100");
  }

  getSpecificPokemon(name:string): Observable <PokeApiServicesService>{
    return this.http.get<PokeApiServicesService>(baseUrl+name);
  }

  getPokemonById(id:number): Observable <PokeApiServicesService>{
    return this.http.get<PokeApiServicesService>(baseUrl+id);
  }
}
