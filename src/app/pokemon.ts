export interface Pokemon {
    id:number;
    pokedexId:number;
    name:string;
    image:string;
    sprite:string;
    slug:string;
    stats:Stats;
    apiTypes:APIType[];
    apiGeneration:number;
    apiResistances:APIResistance[];
    resistanceModifyingAbilitiesForApi:ResistanceModifyingAbilitiesForAPI;
    apiEvolutions:APIEvolution[];
    apiPreEvolution:string;
    apiResistancesWithAbilities:APIResistance[];
}

export interface APIEvolution {
    name:      string;
    pokedexId: number;
}

export interface APIResistance {
    name:              string;
    damage_multiplier: number;
    damage_relation:   string;
}

export interface APIType {
    name:  string;
    image: string;
}

export interface ResistanceModifyingAbilitiesForAPI {
    name: string;
    slug: string;
}

export interface Stats {
    HP:              number;
    attack:          number;
    defense:         number;
    special_attack:  number;
    special_defense: number;
    speed:           number;
}

export class Pokemon {
    constructor(public id:number , public name:string, public apiGeneration:number, public pokedexId:number,public image:string, public sprite:string,public slug:string
        ,public stats:Stats ,public  apiTypes:APIType[],public apiResistances:APIResistance[],public resistanceModifyingAbilitiesForApi:ResistanceModifyingAbilitiesForAPI, 
        public apiEvolutions:APIEvolution[],public apiPreEvolution:string, public apiResistancesWithAbilities:APIResistance[]){
    }
}
